@extends('layouts.app')

@section('content')
<div class="container">
        <div class="w-full min-h-screen bg-gray-50 flex flex-col sm:justify-center items-center pt-6 sm:pt-0 ">
            <div class="w-full sm:max-w-md p-5 mx-auto rounded  border-white border-2">
                 <h2 class="mb-12 text-center text-5xl font-extrabold text-red-500 ">Welcome.</h2>

             
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row mb-4">
                            <label for="email" class="block mb-1">{{ __('Email Address') }}</label>

                            
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>

                        <div class="row mb-4">
                            <label for="password" class="block mb-1">{{ __('Password') }}</label>

                            
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror  " name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>

                        <div class="mt-6 flex items-center justify-between">
                            <div class="flex items-center">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="mt-6">
                            
                     <button type="submit" class="btn btn-primary w-full inline-flex items-center justify-center px-4 py-2 bg-red-600 border border
                    -transparent rounded-md font-semibold capitalize text-white hover:bg-red-700 active:bg-red-700 focus:outline-none focus:border-red-700 
                        focus:ring focus:ring-red-200 disabled:opacity-25 transition">
                                    {{ __('Login') }}
                                </button>

                             
                            
                        </div>


                        <div class="mt-6 text-center">
                        @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
        <a class="nav-link" href="/register" >Sign up for an account</a>
      </div>
                        
                    </form>
                
            </div>
        </div>
    
</div>
@endsection
